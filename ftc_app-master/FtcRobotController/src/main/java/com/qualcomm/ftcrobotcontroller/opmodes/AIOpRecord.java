package com.qualcomm.ftcrobotcontroller.opmodes;

import android.os.Environment;
import android.renderscript.Sampler;

import com.qualcomm.ftcrobotcontroller.Values;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Benjamin on 2/17/2016.
 */
public class AIOpRecord extends OpMode {

    File servo_climbers_file;
    String servo_climbers_fileName = "/aiopdata-servoClimbers.txt";
    List<Double> list_servo_climbers;
    Servo servo_climbers;

    File servo_climberArm_file;
    String servo_climberArm_fileName = "/aiopdata-servoClimberArm.txt";
    List<Double> list_servo_climberArm;
    Servo servo_climberArm;

    File motor_r_file;
    String motor_r_fileName = "/aiopdata-motorR.txt";
    List<Double> list_motor_r;
    DcMotor motor_r;

    File motor_l_file;
    String motor_l_fileName = "/aiopdata-motorL.txt";
    List<Double> list_motor_l;
    DcMotor motor_l;

    double servo_climbers_speed = Values.servo_climbers_speed;
    double servo_climberArm_speed = Values.servo_climberArm_speed;
    double motorSpeed = Values.motor_movement_speed;

    @Override
    public void init() {
        servo_climbers_file = new File(Environment.getExternalStorageDirectory() + servo_climbers_fileName);
        servo_climbers = hardwareMap.servo.get("servo_climbers");
        list_servo_climbers = new ArrayList<>();

        servo_climberArm_file = new File(Environment.getExternalStorageDirectory() + servo_climberArm_fileName);
        servo_climberArm = hardwareMap.servo.get("servo_climberArm");
        list_servo_climberArm = new ArrayList<>();

        /*motor_r_file = new File(Environment.getExternalStorageDirectory() + motor_r_fileName);
        motor_r1 = hardwareMap.dcMotor.get("motor_r1");
        list_motor_r = new ArrayList<>();

        motor_l_file = new File(Environment.getExternalStorageDirectory() + motor_l_fileName);
        motor_l1 = hardwareMap.dcMotor.get("motor_l1");
        list_motor_l = new ArrayList<>();*/
    }

    @Override
    public void loop() {

        if (time > .1)
        {
            list_servo_climbers.add(servo_climbers.getPosition());
            list_servo_climberArm.add(servo_climberArm.getPosition());
            //list_motor_r.add(motor_r1.getPower());
            //list_motor_l.add(motor_l1.getPower());
            resetStartTime();
        }

        if (gamepad1.x)
        {
            servo_climbers.setPosition(Range.clip(servo_climbers.getPosition()-servo_climbers_speed,0,1));
        }
        else if (gamepad1.b)
        {
            servo_climbers.setPosition(Range.clip(servo_climbers.getPosition()+servo_climbers_speed,0,1));
        }

        if (gamepad1.y)
        {
            servo_climberArm.setPosition(Range.clip(servo_climberArm.getPosition()+ servo_climberArm_speed,0,1));
        }
        else if (gamepad1.a)
        {
            servo_climberArm.setPosition(Range.clip(servo_climberArm.getPosition()- servo_climberArm_speed,0,1));
        }

        //Movement();
    }

    void Movement()
    {
        double throttle = gamepad1.left_stick_y;
        double direction = gamepad1.left_stick_x;

        double right = Range.clip((throttle - direction)*motorSpeed, -1, 1);
        double left = Range.clip((throttle + direction)*motorSpeed, -1, 1);

        motor_r.setPower(right);
        motor_l.setPower(left);
    }

    @Override
    public void stop() {
        WriteData(servo_climbers_file, list_servo_climbers);
        WriteData(servo_climberArm_file, list_servo_climberArm);
        //WriteData(motor_r_file, list_motor_r);
        //WriteData(motor_l_file, list_motor_l);
    }

    void WriteData(File f, List<Double> l)
    {
        try
        {
            Writer output = new BufferedWriter(new FileWriter(f));
            for (Double x : l)
            {
                output.append(x.toString() + "\n");
            }
            output.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
