package com.qualcomm.ftcrobotcontroller;

/**
 * Created by Benjamin on 2/18/2016.
 */
public class Values
{
    public static double servo_climbers_speed = .01;
    public static double servo_climberArm_speed = .005;
    public static double motor_movement_speed = 1;
    public static double motor_armAngle_speed = .3;
    public static double motor_armExtend_speed = 1;
    public static double gamepadTriggerThreshold = .1;
}
