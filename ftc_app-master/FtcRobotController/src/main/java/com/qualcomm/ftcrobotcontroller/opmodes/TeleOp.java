package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.ftcrobotcontroller.Values;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

/**
 * Created by Benjamin on 2/17/2016.
 */
public class TeleOp extends OpMode {

    DcMotor motor_r1;
    //DcMotor motor_r2;
    DcMotor motor_l1;
    //DcMotor motor_l2;

    DcMotor motor_armAngle;

    DcMotor motor_armExtend;

    Servo servo_climbers;
    Servo servo_climberArm;

    double servo_climbers_speed = Values.servo_climbers_speed;
    double servo_climberArm_speed = Values.servo_climberArm_speed;
    double motor_movement_speed = Values.motor_movement_speed;
    double motor_armAngle_speed = Values.motor_armAngle_speed;
    double motor_arm_extend_speed = Values.motor_armExtend_speed;

    double gamepadTriggerThreshold = Values.gamepadTriggerThreshold;

    boolean hang = false;
    double hangAmount = .1;

    @Override
    public void init() {
        motor_r1 = hardwareMap.dcMotor.get("motor_r1");
        //motor_r2 = hardwareMap.dcMotor.get("motor_r2");
        motor_l1 = hardwareMap.dcMotor.get("motor_l1");
        //motor_l2 = hardwareMap.dcMotor.get("motor_l2");

        motor_armAngle = hardwareMap.dcMotor.get("motor_armAngle");
        motor_armExtend = hardwareMap.dcMotor.get("motor_armExtend");

        servo_climbers = hardwareMap.servo.get("servo_climbers");
        servo_climberArm = hardwareMap.servo.get("servo_climberArm");
    }

    @Override
    public void loop() {
        if (gamepad1.x)
        {
            servo_climbers.setPosition(Range.clip(servo_climbers.getPosition()-servo_climbers_speed,0,1));
        }
        else if (gamepad1.b)
        {
            servo_climbers.setPosition(Range.clip(servo_climbers.getPosition()+servo_climbers_speed,0,1));
        }

        if (gamepad1.y)
        {
            servo_climberArm.setPosition(Range.clip(servo_climberArm.getPosition() + servo_climberArm_speed, 0, 1));
        }
        else if (gamepad1.a)
        {
            servo_climberArm.setPosition(Range.clip(servo_climberArm.getPosition() - servo_climberArm_speed, 0, 1));
        }

        if (gamepad1.left_bumper)
        {
            hang = true;
        }

        Movement();
        Arm();
    }

    void Movement()
    {
        double throttle = gamepad1.left_stick_x;
        double direction = gamepad1.left_stick_y;

        double right = Range.clip((throttle - direction)* motor_movement_speed, -1, 1);
        double left = Range.clip((throttle + direction)* motor_movement_speed, -1, 1);

        motor_r1.setPower(right);
        //motor_r2.setPower(right);
        motor_l1.setPower(left);
        //motor_l2.setPower(left);
    }

    void Arm()
    {
        double speed = Range.clip(-gamepad1.right_stick_y*motor_armAngle_speed - .2,-1,1);
        motor_armAngle.setPower(speed);

        if (gamepad1.right_trigger >= gamepadTriggerThreshold)
        {
            motor_armExtend.setPower(motor_arm_extend_speed);
        }
        else if (gamepad1.left_trigger >= gamepadTriggerThreshold)
        {
            motor_armExtend.setPower(-motor_arm_extend_speed);
        }
        else if (!hang)
        {
            motor_armExtend.setPower(0);
        }
        else
        {
            motor_armExtend.setPower(hangAmount);
        }
    }
}
