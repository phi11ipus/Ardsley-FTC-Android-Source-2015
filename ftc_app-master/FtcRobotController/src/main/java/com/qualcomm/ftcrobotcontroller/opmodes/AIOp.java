package com.qualcomm.ftcrobotcontroller.opmodes;

import android.content.Context;
import android.os.Environment;

import com.qualcomm.ftcrobotcontroller.FtcRobotControllerActivity;
import com.qualcomm.ftcrobotcontroller.Values;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Benjamin on 2/17/2016.
 */
public class AIOp extends OpMode {

    File servo_climbers_file;
    String servo_climbers_fileName = "/aiopdata-servoClimbers.txt";
    List<Double> list_servo_climbers;
    Servo servo_climbers;

    File servo_climberArm_file;
    String servo_climberArm_fileName = "/aiopdata-servoClimberArm.txt";
    List<Double> list_servo_climberArm;
    Servo servo_climberArm;

    File motor_r_file;
    String motor_r_fileName = "/aiopdata-motorR.txt";
    List<Double> list_motor_r;
    DcMotor motor_r;

    File motor_l_file;
    String motor_l_fileName = "/aiopdata-motorL.txt";
    List<Double> list_motor_l;
    DcMotor motor_l;

    int step = 0;
    boolean run = true;

    @Override
    public void init() {
        servo_climbers_file = new File(Environment.getExternalStorageDirectory() + servo_climbers_fileName);
        servo_climbers = hardwareMap.servo.get("servo_climbers");
        list_servo_climbers = new ArrayList<>();

        servo_climberArm_file = new File(Environment.getExternalStorageDirectory() + servo_climberArm_fileName);
        servo_climberArm = hardwareMap.servo.get("servo_climberArm");
        list_servo_climberArm = new ArrayList<>();

        /*motor_r_file = new File(Environment.getExternalStorageDirectory() + motor_r_fileName);
        motor_r1 = hardwareMap.dcMotor.get("motor_r1");
        list_motor_r = new ArrayList<>();

        motor_l_file = new File(Environment.getExternalStorageDirectory() + motor_l_fileName);
        motor_l1 = hardwareMap.dcMotor.get("motor_l1");
        list_motor_l = new ArrayList<>();*/

        ReadData(servo_climbers_file, list_servo_climbers);
        ReadData(servo_climberArm_file, list_servo_climberArm);
        //ReadData(motor_r_file, list_motor_r);
        //ReadData(motor_l_file, list_motor_l);
    }

    @Override
    public void loop()
    {
        if (time > .1 && run)
        {
            try {
                servo_climbers.setPosition(list_servo_climbers.get(step));
                servo_climberArm.setPosition(list_servo_climberArm.get(step));
                //motor_r1.setPower(list_motor_r.get(step));
                //motor_l1.setPower(list_motor_l.get(step));
            }
            catch (IndexOutOfBoundsException e)
            {
                e.printStackTrace();
                servo_climbers.setPosition(0);
                servo_climberArm.setPosition(0);
                //motor_r1.setPower(0);
                //motor_l1.setPower(0);
                run = false;
            }

            step++;
            resetStartTime();
        }
    }
    
    void ReadData(File f, List<Double> l)
    {
        try {
            FileInputStream inputStream = new FileInputStream(f);
            InputStreamReader in = new InputStreamReader(inputStream);
            BufferedReader reader = new BufferedReader(in);

            String line = null;

            while ((line = reader.readLine()) != null)
            {
                Double x = Double.parseDouble(line);
                l.add(x);
            }

            reader.close();
            in.close();
            inputStream.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
